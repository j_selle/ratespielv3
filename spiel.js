document.addEventListener("DOMContentLoaded", function() {
// Global variables
    var lösung;
    var versuche;

    //Functions
    function start() {
        lösung = Math.floor(Math.random() * 10) + 1;
        versuche = 0;
        hideElements(["gewonnen", "verloren", "neustart"]);
        showElements(["tipp"]);
        alterText('hinweis', "");
        alterText('zahl', "");
    }

    function hideElements(tagArray) {
        for (var tag of tagArray) {
            if (!document.getElementById(tag).classList.contains('hidden')) {
                document.getElementById(tag).classList.add('hidden');
            }
        }
    }

    function showElements(tagArray) {
        for (var tag of tagArray) {
            document.getElementById(tag).classList.remove('hidden');
        }
    }

    function alterText(tag, text) {
        document.getElementById(tag).textContent = text;
    }


    function raten() {
        alterText('zahl', 'Zahl: ' + lösung);
        versuche += 1;
        var eingabe = document.querySelector('input').value;
        eingabe = parseInt(eingabe);
        console.log(eingabe);1
        if (versuche < 3) {
            if (eingabe < lösung) {
                alterText('hinweis', 'Zu klein!');
            } else if (eingabe > lösung) {
                alterText('hinweis', 'Zu groß!');
            } else {
                showElements(["gewonnen", "neustart"]);
                hideElements(['tipp']);
                alterText('hinweis', "");
            }
        }
        else {
            showElements(["verloren", "neustart"]);
            hideElements(['tipp']);
}
}

    //Execute Programm
    start();
    document.getElementById('neustart').addEventListener('click', start);
    document.getElementById('tipp').addEventListener('click', raten);
    document.querySelector('input').addEventListener('keyup', function (e) {
        if (e.keyCode === 13) {
            raten();
        }
    });
});